# README #

This project aims to create a long running container infrastructure to automaticly render tiled maps of Sentinale S2A & S2B Products published at https://scihub.esa.int

### What is this repository for? ###

* based of the entries in the docker-compose.yml one or more instances are started. One Container for one Lon/Lat
* Containers checking every hour if there are Products of the defined Coordinates and downloads them
* Products are then rendered to georeferenced RGB TIFs from the Layers B02, B03, B04 
* then maptiles are rendered from the RGB TIF. you will find *.html in the forderstructure, ready to use in your browser.
* all data is shared on http://<hostip>:15000

### How do I get set up? ###

* a machine running docker https://docs.docker.com/engine/installation/linux/docker-ce/debian/
* to start the containers you will then need docker-compose https://docs.docker.com/compose/install/#install-compose
* git to check out this repro
* a Account at ESAs scihub[sic] https://scihub.esa.int/
* edit the docker-compose.yml and change the settings to your needs. (see Annotations in this file)
* finally, start this pixelcrusher in the repro rootdir with:  docker-compose up -d --build
* first start takes a long time. depending of the Date settings. all already avaible products are downloaded and rendered
* after that, scihub is checked every hour of a new product and every 10 minutes the container is checking for then new products to rendered as map

### TODO ###

* creating a landing page for all Images

### its Beta ###
* please note that this is in an early stage and everything can change :)


### used software ###
* docker https://docs.docker.com/engine/installation/
* docker-compose https://docs.docker.com/compose/install/
* ESA SNAP Toolbox http://step.esa.int/main/download/
* gdal http://www.gdal.org/
* Sentinel-download (special thx) https://github.com/olivierhagolle/Sentinel-download