#!/usr/bin/env python

import schedule
import time
import subprocess


# download products and unzip them to /data/products/<product_name>.SAFE/
def download_new_products():
    print("I'm checking for new products ...")
    cmd =['/holhooja/download.sh']
    p = subprocess.call(cmd,stdout=True) # True, if the stdout of download script to docker logs
    if p == 0:
        cmd = ['/usr/bin/unzip', '-n','-d', '/data/products/', '/data/*.zip']
        return subprocess.call(cmd, stdout=True)  # True, if the stdout of download script to docker logs
    else:
        return 1


def render_products():
    print("start rendering products")
    cmd =['/holhooja/render.py']
    p = subprocess.call(cmd, stdout=True)  # True, if the stdout of download script to docker logs


# first on container start
download_new_products()
render_products()

schedule.every().hour.do(download_new_products)
schedule.every(10).minutes.do(render_products)

while True:
    schedule.run_pending()
    time.sleep(1)


