#!/bin/bash

echo "starting to download";
echo "$SCIHUB_USER $SCIHUB_PASS">/apihub.txt;
/Sentinel-download/Sentinel_download.py --lat $SCIHUB_LAT --lon $SCIHUB_LON -a /apihub.txt -d $SCIHUB_ACQUIRED_AFTER -f $SCIHUB_ACQUIRED_BEFORE -w /data && \
exit 0;
