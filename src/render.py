#!/usr/bin/env python3

import subprocess
import fnmatch
import os


PIDFILE = '/tmp/render_is_running'

if os.path.isfile(PIDFILE):
    print('PIDFILE %s exsists, exiting' % PIDFILE)
    exit(0)
else:
    with open(PIDFILE, 'w') as fp:
        fp.write(':)')


def render_tif(productdir,targetdir, layerlist='4,3,2', pconvert_bin = '/usr/local/snap/bin/pconvert'):
    print("rendering product in", productdir, "to", targetdir)
    if not os.path.isdir(targetdir):
        os.mkdir(targetdir)
    cmd = [pconvert_bin, '-b', layerlist, '-f', 'TIF', '-o', targetdir, productdir]
    return subprocess.call(cmd, stdout=True)


def render_tiles_from_tif(tif, targetdir):
    cmd = ['/usr/bin/gdal2tiles.py', '-s_srs', 'EPSG:4326 ', tif, targetdir]
    print(cmd)
    return subprocess.call(cmd, stdout=True)


dirpath = '/data/products/'
try:
    matches = []
    for root, dirnames, filenames in os.walk(dirpath):
        for filename in fnmatch.filter(filenames, 'manifest.safe'):
            rdir = os.path.join(root, 'holhooja/')
            imagefilename = os.path.join(root, 'holhooja/'+root.split('/')[-1].replace('.SAFE', '.TIF'))
            if os.path.isfile(imagefilename):
                print ("render result ", imagefilename, "exists")
                tilemapresource = os.path.join(rdir, 'tilemapresource.xml')
                if not os.path.isfile(tilemapresource):
                    render_tiles_from_tif(imagefilename, rdir)

                    # fix layer opacity of 0.7 in html. need a way to set this with cli option
                    map_html = os.path.join(rdir, 'openlayers.html')
                    print('patching layer opacity to 1.0 in map %s' % map_html)
                    with open(map_html, 'r') as fp:
                        map = fp.read().replace('tmsoverlay.setOpacity(0.7)', 'tmsoverlay.setOpacity(1.0)')
                    with open(map_html, 'w') as fp:
                        fp.write(map)
                else:
                    print('"%s" exsists, looks like we already have the titles' % tilemapresource)
            else:
                render_tif(root, rdir)
except Exception as e:
    print (e)
    os.remove(PIDFILE)
    exit(1)

os.remove(PIDFILE)
exit(0)

