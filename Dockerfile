FROM python:latest
MAINTAINER 3c5x9 <3c5x9@gmx.net>
ENV VERSION 2 20180513

#####
# install APT and Python Deps
#####
RUN apt-get update && apt-get -y install nano htop curl git unzip tree && apt-get clean
ADD requirements.txt /
RUN pip install --upgrade pip && pip install -r /requirements.txt
#####
# install ESA SNAP s2tbx
#####
RUN apt-get update && apt-get -y install wget && \
 wget http://step.esa.int/downloads/6.0/installers/esa-snap_sentinel_unix_6_0.sh && \
 chmod 775 esa-snap_sentinel_unix_6_0.sh && \
 ./esa-snap_sentinel_unix_6_0.sh -q && \
 rm esa-snap_sentinel_unix_6_0.sh

#####
# install ESA sen2cor
# Install http://step.esa.int/thirdparties/sen2cor/2.5.5/docs/S2-PDGS-MPC-L2A-SRN-V2.5.5.pdf
# Config and Manual http://step.esa.int/thirdparties/sen2cor/2.5.5/docs/S2-PDGS-MPC-L2A-SUM-V2.5.5_V2.pdf
#####
RUN wget http://step.esa.int/thirdparties/sen2cor/2.5.5/Sen2Cor-02.05.05-Linux64.run && \
  bash Sen2Cor-02.05.05-Linux64.run --target /sen2cor/ && \
  /sen2cor/bin/L2A_Process --help && \
  rm Sen2Cor-02.05.05-Linux64.run

#####
# install gdal
#####
RUN apt-get install -y gdal-bin python-gdal

####
# install download helper
####
RUN git clone https://github.com/olivierhagolle/Sentinel-download.git

# creating dir with scripts
RUN mkdir -p /holhooja
COPY src/ /holhooja/
COPY container_entry.sh /

RUN chmod 775 /holhooja/*.py /holhooja/*.sh /container_entry.sh

WORKDIR /holhooja
VOLUME /data

# to get nano working
ENV TERM xterm

# scihub.copernicus.eu User Creds
ENV SCIHUB_USER scihub_username
ENV SCIHUB_PASS scihub_password
ENV SCIHUB_LON 44.0
ENV SCIHUB_LAT 53.0
ENV SCIHUB_ACQUIRED_AFTER 20171101
ENV SCIHUB_ACQUIRED_BEFORE 20991231


#
CMD /container_entry.sh

